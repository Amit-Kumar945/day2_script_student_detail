#!/bin/bash
student_marksheet() {
	csv_file="$1"

#Total_no_of_student_in_csv_file

total_student=`awk 'BEGIN{i=1}{i++;}END{print expr i-1 - 1 }' $csv_file`
echo "Total no of student = $total_student"

#list variable store the name and percentage of all student
list=`awk -F "," 'NR == 1 { print $1, "Percentage"; next }
NF > 1 {
for(i=1; i<=NF;i++) 
	j+=$i; 
	print $1 "," expr j / 3;j=0 }' $csv_file`

first=`echo "$list" | awk -F "," 'NR > 1 { if ($2 >= 60 && $2 <= 80 ) print $1 "," "First Division"}'`
second=`echo "$list" | awk -F "," 'NR > 1 { if ($2 >= 55 && $2 <= 59 ) print $1 "," "Second Division"}'`
third=`echo "$list" | awk -F "," 'NR > 1 { if ($2 >= 35 && $2 <=54  ) print $1 "," "Third Division"}'`
fail=`echo "$list" | awk -F "," 'NR > 1 { if ( $2 <= 34 ) print $1 "," "Fail"}'`
distinction=`echo "$list" | awk -F "," 'NR > 1 { if ($2 >= 81 ) print $1 "," "Distinction"}'`

}

